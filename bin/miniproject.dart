import 'dart:io';
import 'dart:math';

class Living {
  String name = '';
  int hp = 15;
  int atk = 1;
  String status = 'Alive';

  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  int getHP() {
    return hp;
  }

  void setHP(int newHp) {
    if (newHp == 1) {
      hp = hp - 1;
    } else if (newHp == 2) {
      hp = hp - 2;
    } else if (newHp == 3) {
      hp = hp - 3;
    }
    if (hp <= 0) {
      hp = 0;
      status = 'Dead';
    }
  }

  int getATK() {
    return atk;
  }

  void setATK(int newAtk) {
    // ATK = atk;
    if (newAtk == 1) {
      atk = 1;
    } else if (newAtk == 2) {
      atk = 2;
    } else if (newAtk == 3) {
      atk = 3;
    }
  }

  String getStatus() {
    return status;
  }

  void setStatus(String status) {
    this.status = status;
  }
}

class Question {
  List<int> numbers = [Random().nextInt(10), Random().nextInt(10)];
  String? operation;
  int? answer;

  Question() {
    final List<String> operations = ['+', '-', '*'];
    int? ans;
    List<int> nums = numbers;
    // generate a random operation
    int opIdx = Random().nextInt(3);
    switch (opIdx) {
      case 0:
        ans = nums[0] + nums[1];
        break;
      case 1:
        ans = nums[0] - nums[1];
        break;
      case 2:
        ans = nums[0] * nums[1];
        break;
    }
    operation = operations[opIdx];
    answer = ans;
  }
  // String expression() => "${numbers[0]} $operation ${numbers[1]} = ?";

  getNum() {
    return numbers;
  }

  getOperation() {
    return operation;
  }

  getResult() {
    return answer;
  }

  @override
  String toString() {
    return '${numbers[0]} $operation ${numbers[1]} = ?';
  }
}

class Game {
  Question question = Question();
  Living player = Living();
  Living monster = Living();
  bool win = false;
  int countP = 0;
  int countM = 0;
  var reset = 'n';
  bool isWin() {
    return win;
  }

  void showWelcome() {
    print('Welcome to Battle Game!');
    print('Please input your name');
  }

  void showPlayer() {
    String name = stdin.readLineSync()!;
    player.setName(name);
    print('Welcome to Battle Game : $name !!');
  }

  void showDefault() {
    print('${player.getName()} HP : ${player.getHP()}');
    print('ATK : ${player.getATK()}');
    print('Status : ${player.getStatus()}');
    print('\n');
    print('Monster(AI) HP : ${monster.getHP()}');
    print('ATK : ${monster.getATK()}');
    print('Status : ${monster.getStatus()}');
  }

  void showSolution() {
    print('\n');
    print('Please input your answer until you win or lose.');
    Question question = Question();
    print(question.toString());
    int inputAns = int.parse(stdin.readLineSync()!);
    if (inputAns == question.getResult()) {
      showPlayerScore();
    } else {
      showMonsterScore();
    }
  }

  void showPlayerScore() {
    print('Good Job!!');
    if (countP <= 1) {
      player.setATK(1);
      monster.setHP(1);
      countP++;
    } else if (countP == 2) {
      player.setATK(2);
      monster.setHP(2);
      countP++;
    } else if (countP >= 3) {
      player.setATK(3);
      monster.setHP(3);
      countP++;
    }
    checkHPPlayer();
  }

  void showMonsterScore() {
    print('Try Harder!');
    if (countM <= 1) {
      monster.setATK(1);
      player.setHP(1);
      countM++;
    } else if (countM == 2) {
      monster.setATK(2);
      player.setHP(2);
      countM++;
    } else if (countM >= 3) {
      monster.setATK(3);
      player.setHP(3);
      countM++;
    }
    checkHPMonster();
  }

  bool isFinish() {
    if (checkHPPlayer()) {
      return true;
    } else if (checkHPMonster()) {
      return true;
    }
    return false;
  }

  bool checkHPPlayer() {
    if (player.getHP() == 0) {
      return true;
    }
    return false;
  }

  bool checkHPMonster() {
    if (monster.getHP() == 0) {
      return true;
    }
    return false;
  }

  void showResult() {
    if (checkHPMonster()) {
      print('You Win!');
    } else if (checkHPPlayer()) {
      print('You Lost!');
    }
  }

  bool doContinue() {
    print("Play again? (y/n):");
    reset = stdin.readLineSync()!;
    if (reset == 'y') {
      return true;
    } else if (reset == 'n') {
      print('Good bye!');
      return false;
    }
    return true;
  }

  void newGame() {
    player.hp = 15;
    player.atk = 1;
    player.status = 'Alive';
    monster.hp = 15;
    monster.atk = 1;
    monster.status = 'Alive';

    print('${player.getName()} HP : ${player.getHP()}');
    print('ATK : ${player.getATK()}');
    print('Status : ${player.getStatus()}');
    print('\n');
    print('Monster(AI) HP : ${monster.getHP()}');
    print('ATK : ${monster.getATK()}');
    print('Status : ${monster.getStatus()}');
  }
}

void main() {
  Game game = Game();
  game.showWelcome();
  game.showPlayer();
  game.showDefault();
  while (true) {
    game.showSolution();
    game.showDefault();
    if (game.isFinish()) {
      game.showResult();
      if (game.doContinue()) {
          game.newGame();
      }else {
        break;
      }
    }
  }
}
